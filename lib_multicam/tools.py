import json
from lib_multicam.sql import *
from vininfo import Vin
from lib_multicam.manufacture_dict import WMI
import logging


class Sql:
    def __init__(self, cfg, name_table, logger=None) -> None:
        self.cfg = cfg
        self.name_table = name_table
        self.create_vin_table = f"""
        CREATE TABLE {self.name_table} (
        Timestamp VARCHAR(80) NOT NULL,
        CameraID VARCHAR(80) NOT NULL,
        Result VARCHAR(80) NOT NULL,
        Result_Length VARCHAR(80) NOT NULL,
        Conf_OCR VARCHAR(80) NOT NULL,
        Conf_Det VARCHAR(80) NOT NULL,
        Bounding_boxes VARCHAR(80) NOT NULL,
        PRIMARY KEY(Timestamp,CameraID)
        );"""
        self.logger = logger
        self.create_db_connection()

    def create_db_connection(self):
        self.connection = create_db_connection(
            self.cfg["host_name"], self.cfg["user_name"], self.cfg["user_password"], self.cfg["db_name"], self.logger)  # Connect to the Database
        # self.create_table_boolean = execute_query(
        #     self.connection, self.create_vin_table)
        if self.cfg["clear_db"]:
            print("clear data base")
            execute_query(self.connection, f"DELETE FROM {self.name_table};" , self.logger)

    def insert_table_sql(self, best_result, id_cam):
        bb = json.dumps(best_result[3].tolist())
        sql = f"INSERT INTO {self.name_table} VALUES (%s, %s, %s, %s, %s, %s, %s)"
        val = [(best_result[-1], id_cam, str(best_result[1]), str(len(best_result[1])),
                str(round(best_result[2], 4)), str(round(best_result[4], 4)), bb)]
        pop = execute_list_query(self.connection, sql, val, self.logger)
        return pop


def get_best_result(list_last_idcar, dict_car_tracking, dict_idcam_start, max_e=6, logger=None):
    """get best result and visualize list image of one car

    Args:
        list_last_idcar (list): list id car want to save
        dict_car_tracking (dict): dict used to save value of one car
        dict_idcam_start (dict): dict used to save which cam is checking
        root_out (str): path folder out to save output
    """
    best_result_list = []
    best_result_topk_list = []
    id_cam_list = []
    value_list = []
    for id_car in list_last_idcar:
        id_cam = id_car.split("_")[0]
        value = dict_car_tracking.pop(id_car)
        # output_image, ocr, score_ocr, box, score_Detect, timestamp
        best_result, best_result_topk = find_best_result(value, max_e, logger)
        # SQL SAVE
        best_result_list.append(best_result)
        best_result_topk_list.append(best_result_topk)
        id_cam_list.append(id_cam)
        value_list.append(value)

    return best_result_list, best_result_topk_list, id_cam_list, value_list


def clean_string(string):
    if string is None:
        return ""
    ocr_combined = ""
    for letter in string:
        if letter.isalnum():
            if letter.upper() in ["I"]:
                letter = "1"
            elif letter.upper() in ["O", "Q"]:
                letter = "0"
            ocr_combined += letter
    ocr_combined = clean_first_character(ocr_combined)
    return ocr_combined


def checksum_with_vin(string):
    if len(string) != 17:
        return False

    return Vin(string).verify_checksum()


def clean_first_character(vin):
    vin = vin.upper()
    vin_head, vin_tail = vin[:3], vin[3:]
    for head in WMI.keys():
        if head[0] in ["1", "T"]:
            if head[1:] == vin_head[1:] and checksum_with_vin(head + vin_tail):
                return head + vin_tail

    return vin


def find_best_result(value, max_e=6, logger=None):
    vote_dict = {}
    conf_dict = {}
    # print()
    for det in value:
        ocr_result = det[-5]
        if vote_dict.get(ocr_result):
            vote_dict[ocr_result] += 1
        else:
            vote_dict[ocr_result] = 1
        if ((ocr_result not in conf_dict)
                or (det[-4] > conf_dict[clean_string(ocr_result)])):  # and len(clean_string(ocr_result)) > 10:

            conf_dict[clean_string(ocr_result)] = det[-4]

    sorted_dict = {k: v for k, v in sorted(
        vote_dict.items(), key=lambda item: item[1], reverse=True)}
    sorted_key_iter = iter(sorted_dict.items())
    # if logger:
    #     logger.info(sorted_dict)
    #     logger.info(conf_dict)
    ocr_list = []
    topk_result = []
    count = 0
    for i in range(len(sorted_dict)):
        ocr, ocr_count = next(sorted_key_iter)
        ocr = clean_string(ocr)
        if len(ocr) == 17:
            if count == 0 or ocr_count == count:
                ocr_list.append(ocr)
                count = ocr_count
            # vin_boolean = Vin(ocr).verify_checksum()
            # if vin_boolean:
            #     ocr_list = [ocr]
            #     break
        if len(ocr) > 15:
            if ocr not in topk_result:
                topk_result.append(ocr)

    if not ocr_list:
        ocr_list = [list(sorted_dict.keys())[0]]

    if not topk_result:
        topk_result = [list(sorted_dict.keys())[0]]

    max_conf = 0
    for ocr in ocr_list:
        if conf_dict[ocr] > max_conf:
            final_ocr = ocr
    output = []
    image = None
    if final_ocr:
        for det in value:
            if clean_string(det[-5]) == final_ocr:
                det[-5] = clean_string(det[-5])
                output.append(det)
                if len(det) == max_e:
                    image = det[0]
                    break
        if image is None:
            image = [det[0] for det in value if len(det) == max_e][0]

        output = sorted(output, key=lambda item: item[-5], reverse=True)[0]
        if len(output) != max_e:
            output = [image] + output

    output_topk = []
    if topk_result:
        for result in topk_result:
            for det in value:
                if clean_string(det[-5]) == result:
                    det[-5] = clean_string(det[-5])
                    if len(det) != max_e:
                        det = [None] + det
                    output_topk.append(det)
                    break

    if output_topk:
        if output:
            return output, output_topk
        else:
            return None, output_topk
    else:
        return None, None


def Log(name_file) -> None:
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.FileHandler(name_file)
    # ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)
    return logger
