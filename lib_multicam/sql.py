import mysql.connector
from mysql.connector import Error
import pandas as pd

def create_server_connection(host_name, user_name, user_password, logger):
    connection = None
    try:
        connection = mysql.connector.connect(
            host=host_name,
            user=user_name,
            passwd=user_password
        )
        print("MySQL Database connection successful")
    except Error as err:
        print(f"Error: '{err}'")
        logger.debug(f"Program kill, check fail:  {err}")
        logger.debug("NEED_RESTART")
    return connection

def create_database(connection, query, logger):
    try:
        cursor = connection.cursor()
        cursor.execute(query)
        print("Database created successfully")
    except Error as err:
        print(f"Error: '{err}'")
        logger.debug(f"Program kill, check fail:  {err}")
        logger.debug("NEED_RESTART")

def create_db_connection(host_name, user_name, user_password, db_name, logger):
    connection = None
    try:
        connection = mysql.connector.connect(
            host=host_name,
            user=user_name,
            passwd=user_password,
            database=db_name
        )
        print("MySQL Database connection successful")
    except Error as err:
        print(f"Error: '{err}'")
        logger.debug(f"Program kill, check fail:  {err}")
        logger.debug("NEED_RESTART")

    return connection

def execute_query(connection, query, logger):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        print("Query successful")
        return True
    except Error as err:
        print(f"Error: '{err}'")
        logger.debug(f"Program kill, check fail:  {err}")
        logger.debug("NEED_RESTART")
        return False

def execute_list_query(connection, sql, val, logger):
    try:
        cursor = connection.cursor()
        cursor.executemany(sql, val)
        connection.commit()
        # print("Query successful")
        if logger:
            logger.info(f"Query successful on {val[0][1]} {val[0][2]}")
        return True
    except Error as err:
        if logger:
            logger.debug(f"Program kill, check fail:  {err}")
            logger.debug("NEED_RESTART")
        # print(f"Error: '{err}'")
        return False