from threading import Thread
import cv2
from queue import Queue
import threading
import datetime
import os, time
import numpy  as np
from utils.cv2_puttext_wrapper import draw_text

class Queue(Queue):
    """Add clear queue 

    Args:
        Queue (_type_): Queue class
    """
    def clear_queue(self):
        for i in range(len(self.queue)):
            self.get()

def make_dir(path):
    if not os.path.exists(path):
        os.mkdir(path)

class SqlQueue:
    """Read stream using multithread
    """
    def __init__(self, sqlQueue, name_Thread="None"):
        """ init param

        Args:
            workQueue (queue): queue use to save frame
            src (int, optional): source camera. Defaults to 0.
            name_Thread (str, optional): name of thread. Defaults to "None".
            skip_frame (int, optional): number frame want to skip. Defaults to 10.
        """
        self.stopped = False
        self.name_Thread = name_Thread
        self.sqlQueue = sqlQueue
        self.queueLock = threading.Lock()

    def start(self):
        """start a thread

        Returns:
            _type_: _description_
        """
        # start the thread to read frames from the video stream
        thread = Thread(target=self.update, args=())
        thread.start()
        return self, thread

    def update(self):
        i = 0
        while True:
            time.sleep(0.5)
            # if the thread indicator variable is set, stop the thread
            if self.stopped:
                return self.name_Thread
            self.queueLock.acquire()
            if self.sqlQueue.full():
                self.sqlQueue.get()

            self.save()

            self.queueLock.release()
            i += 1
            # if i > 5000:
            #     self.stop()

    def save(self):
        # [list_out_image[idx], ocr_output[idx], list_score_ocr[idx], list_box[idx], list_score_detect[idx], ct]
        list_queue = self.take_queue(1)
        if list_queue:
            best_result, id_cam, value, id_car, Sql_VIN, root_out, visualize = list_queue[0]

            Sql_VIN.insert_table_sql(best_result, id_cam)

            if visualize:
                img = best_result[0].copy()
                img = cv2.polylines(img, np.int32(
                    [best_result[-3]]), True, color=(0, 0, 255), thickness=2)
                draw_txt = "ocr: " + best_result[1] + "\n"
                draw_txt += "ocr_conf: " + str(best_result[2]) + "\n"
                draw_txt += "det_conf: " + str( best_result[-2])
                img = draw_text(img, draw_txt, tuple(
                    np.int32(best_result[-3][1])), fontScale=1, color=(0, 0, 255), thickness=2)
                path_out_best = os.path.join(root_out, "best_result")
                path_out_visualize = os.path.join(root_out, "visualize")
                for path in [path_out_best, path_out_visualize]:
                    make_dir(path)
                # save best image
                best_ocr = best_result[1]
                # print("id_car", id_car)
                basename = id_car.split("_")[0] + "_" + best_result[-1] + f"_{best_ocr}.jpg"
                cv2.imwrite(os.path.join(path_out_best, basename), img)
                path_out_folder = os.path.join(path_out_visualize, basename[:-4])
                make_dir(path_out_folder)
                for item in value:
                    if len(item) == 6:
                        img = item[0].copy()
                        img = cv2.polylines(img, np.int32(
                        [item[-3]]), True, color=(0, 0, 255), thickness=2)
                        draw_txt = "ocr: " + item[1] + "\n"
                        draw_txt += "ocr_conf: " + str(item[2]) + "\n"
                        draw_txt += "det_conf: " + str( item[-2])
                        img = draw_text(img, draw_txt, tuple(
                        np.int32(item[-3][1])), fontScale=1, color=(0, 0, 255), thickness=2)
                        date_string = datetime.datetime.now()  # .strftime("%Y-%m-%d-%H:%M:%S")
                        date_string = str(date_string).replace("-", "").replace(":", "").replace(" ", "")
                        basename = id_car.split("_")[0] + "_" + item[-1] + ".jpg"
                        cv2.imwrite(os.path.join(path_out_folder, basename), img)

    def take_queue(self, numbers):
        """get list value of queue
        Args:
            number (int): number value want to take
        Returns:
            list: list of frame 
        """
        out_list = []
        len_queue = len(self.sqlQueue.queue)
        # print(len_queue)
        # print(len_queue)
        if len_queue < numbers:
            numbers = len_queue

        for i in range(numbers):
            out_list.append(self.sqlQueue.get())

        return out_list
        
    def stop(self):
        """stope read stream
        """
        # indicate that the thread should be stopped
        self.stopped = True

