from threading import Thread
import cv2
from queue import Queue
import threading
import datetime
import time


class Queue(Queue):
	"""Add clear queue 

	Args:
		Queue (_type_): Queue class
	"""

	def clear_queue(self):
		for i in range(len(self.queue)):
			self.get()


class WebcamVideoStream:
	"""Read stream using multithread
	"""
	def __init__(self, vin_conf, workQueue, src=0, name_Thread="None", ROTATE=False, logger=None):
		""" init param

		Args:
			workQueue (queue): queue use to save frame
			src (int, optional): source camera. Defaults to 0.
			name_Thread (str, optional): name of thread. Defaults to "None".
			skip_frame (int, optional): number frame want to skip. Defaults to 10.
		"""
		# initialize the video camera stream and read the first frame
		# from the stream
		self.src = src
		self.stream = cv2.VideoCapture(src)
		self.VIN_conf = vin_conf
		self.skip_frame = self.VIN_conf["skip_frame"]
		self.rotate_camera = ROTATE
		(self.grabbed, self.frame) = self.stream.read()
		# initialize the variable used to indicate if the thread should
		# be stopped
		self.stopped = False
		self.name_Thread = name_Thread
		self.workQueue = workQueue
		self.queueLock = threading.Lock()
		self.PUT_QUEUE = False
		self.logger = logger
		# self.logger_PID = logger[1]


	def update_flag(self):
		self.PUT_QUEUE = True

	def start(self):
		"""start a thread

		Returns:
			_type_: _description_
		"""
		# start the thread to read frames from the video stream
		thread = Thread(target=self.update, args=())
		thread.start()
		return self, thread

	def update(self):
		"""read frame from stream

		Returns:
			_type_: _description_
		"""
		# keep looping infinitely until the thread is stopped
		i = 0
		before_thre = 0
		time_start = time.time()
		while True:
			# if the thread indicator variable is set, stop the thread
			if self.stopped:
				return self.name_Thread
			# otherwise, read the next frame from the stream
			(self.grabbed, self.frame) = self.stream.read()
			if self.grabbed:
				# print("self.skip_frame",self.skip_frame)
				if i % self.skip_frame == 0:
					if self.frame is None:
						continue
					self.queueLock.acquire()
					if self.workQueue.full():
						self.workQueue.get()
						# workQueue.clear_queue()
					# frame, cameraID, timestamp
					# print(f"workQueue.qsize() -------------------------- {self.workQueue.qsize()}")
					ct = datetime.datetime.now()
					if self.rotate_camera:
						self.frame = cv2.rotate(self.frame, cv2.cv2.ROTATE_180)
					if self.PUT_QUEUE:
						self.workQueue.put([self.frame, self.name_Thread, ct])
					self.queueLock.release()
			else:
				if round(time.time() - time_start) % 30 == 0 and round(time.time() - time_start) != before_thre:
					init_time = datetime.datetime.now()
					self.queueLock.acquire()
					self.logger.info(f"Cant read image from {self.name_Thread}")
					self.logger.debug("NEED_RESTART")
					self.queueLock.release()
					before_thre = round(time.time() - time_start)
			i += 1
		
	def read(self):
		"""read single frame

		Returns:
			_type_: _description_
		"""
		# return the frame most recently read
		return self.frame

	def take_queue(self, numbers):

		"""get list value of queue

		Args:
			number (int): number value want to take

		Returns:
			list: list of frame 
		"""

		out_list = []
		len_queue = len(self.workQueue.queue)
		# print(len_queue)
		if len_queue < numbers:
			numbers = len_queue

		for i in range(numbers):
			out_list.append(self.workQueue.get())

		return out_list
		
	def stop(self):
		"""stope read stream
		"""
		# indicate that the thread should be stopped
		self.stopped = True

