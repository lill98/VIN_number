import datetime
from lib_multicam.tools import get_best_result


class Tracking():
    def __init__(self, VIN_config, Sql_VIN, Sql_VIN_topk, sql_object, dict_idcam_start, dict_car_tracking, logger) -> None:
        """init vairable

        Args:
            dict_idcam_start (dict): dict to save start car
            dict_car_tracking (dict): dict to list image of id_car
            number_cache_image (int, optional): number image want to cache. Defaults to 10.
        """
        self.dict_idcam_start = dict_idcam_start
        self.dict_car_tracking = dict_car_tracking
        self.VIN_config = VIN_config
        self.number_cache_image = self.VIN_config["number_cache_image"]
        self.PERCENT_DEALTA_Y = self.VIN_config["PERCENT_DEALTA_Y"]
        self.DEALTA_TIME = self.VIN_config["DEALTA_TIME"]
        self.Sql_VIN = Sql_VIN
        self.Sql_VIN_topk = Sql_VIN_topk
        self.sql_object = sql_object
        self.logger = logger

    def sort_cache(self, input_list, len_e=6):
        """sort image based on len ocr and confidence score

        Args:
            input_list (list): input want to sort
            len_e (int, optional): max len of element in list. Defaults to 5.

        Returns:
            list: list after sort
        """
        sort_list = [i for i in input_list if len(i) == len_e]
        rest_list = [i for i in input_list if len(i) != len_e]
        sorted_list = sorted(sort_list, key=lambda item: len(
            item[1]) + item[2], reverse=True)
        return sorted_list + rest_list

    def convert_second(self, t):
        """Convert format date time to second

        Args:
            t (datetime): datetime want to convert

        Returns:
            int: second
        """
        second = int((t.hour * 60 + t.minute) * 60 + t.second)
        return second

    def take_init_car(self, list_queue, list_box, idx, list_out_image, ocr_output, list_score_ocr, list_score_detect):
        """init tracking, use went first car appear

        Args:
            list_queue (list): list value of queue
            list_box (list): list bounding box  
            idx (int): index of frame in queue
            list_out_image (list): list image after infer
            ocr_output (list): list output ocr of image in queue
            list_score_ocr (list): list of score ocr

        Returns:
            None
        """

        # if cant detect ocr -> skip
        if ocr_output[idx] is None:
            return False

        id_camera = list_queue[idx][1]
        time = list_queue[idx][2]
        y_tl = list_box[idx][0][1]

        # init dict_idcam_start and dict_car_tracking

        if id_camera not in self.dict_idcam_start:
            # time = list_queue[idx][2]
            ct = str(time).replace("-", "").replace(":", "").replace(" ", "") #.split(".")[0]
            key_dict = "_".join([id_camera, ct])
            # time of started car, time of last car, y topleft of last car #, time capture image
            self.dict_idcam_start[id_camera] = [time, time, y_tl]
            self.dict_car_tracking[key_dict] = [[list_out_image[idx], ocr_output[idx], list_score_ocr[idx], list_box[idx], list_score_detect[idx], ct]]

        return True

    def tracking(self, list_queue, list_box, idx, list_out_image, ocr_output, list_score_ocr, list_score_detect):
        """tracking car
        Args:
            list_queue (list): list value of queue
            list_box (list): list bounding box  
            idx (int): index of frame in queue
            list_out_image (list): list image after infer
            ocr_output (list): list output ocr of image in queue
            list_score_ocr (list): list of score ocr

        Returns:
            bool: True if newcar else False
        """

        NEW_CAR = []
        
        if list_out_image[0] is None:
            return NEW_CAR
        else:
            self.DEALTA_Y = int(self.PERCENT_DEALTA_Y*list_out_image[0].shape[1])
        id_camera = list_queue[idx][1]
        time = list_queue[idx][2]
        time_str = str(time).replace("-", "").replace(":", "").replace(" ", "") #.split(".")[0]

        last_time = self.dict_idcam_start[id_camera][1]
        init_time = self.dict_idcam_start[id_camera][0]
        ct = str(init_time).replace("-", "").replace(":", "").replace(" ", "") #.split(".")[0]
        last_y_tl = self.dict_idcam_start[id_camera][2]
        y_tl = list_box[idx][0][1]
        key_car = "_".join([id_camera, ct])

        if (self.convert_second(time) - self.convert_second(last_time)) < self.DEALTA_TIME and abs(y_tl - last_y_tl) < self.DEALTA_Y:
            if len(self.dict_car_tracking[key_car]) < self.number_cache_image:
                self.dict_car_tracking[key_car].append(
                    [list_out_image[idx], ocr_output[idx], list_score_ocr[idx], list_box[idx], list_score_detect[idx], time_str])
            else:
                # sort array based on len(ocr) and confidence score
                self.dict_car_tracking[key_car] = self.sort_cache(
                    self.dict_car_tracking[key_car])
                for i in range(self.number_cache_image - 2, -1, -1):
                    # if last image have condition > input condition -> break, save time
                    if i == self.number_cache_image - 2 \
                            and len(self.dict_car_tracking[key_car][i][1]) + float(self.dict_car_tracking[key_car][i][2]) > len(ocr_output[idx]) + float(list_score_ocr[idx]):
                        break
                    # if first image have condition < input condition -> save input to first
                    elif i == 0 and len(self.dict_car_tracking[key_car][i][1]) + float(self.dict_car_tracking[key_car][i][2]) < len(ocr_output[idx]) + float(list_score_ocr[idx]):
                        del self.dict_car_tracking[key_car][i][0]
                        self.dict_car_tracking[key_car].insert(0,
                            [list_out_image[idx], ocr_output[idx], list_score_ocr[idx], list_box[idx], list_score_detect[idx], time_str])
                            
                        break
                    elif len(self.dict_car_tracking[key_car][i + 1][1]) + float(self.dict_car_tracking[key_car][i + 1][2]) > len(ocr_output[idx]) + float(list_score_ocr[idx]) \
                            > len(self.dict_car_tracking[key_car][i][1]) + float(self.dict_car_tracking[key_car][i][2]):
                        del self.dict_car_tracking[key_car][i][0]
                        self.dict_car_tracking[key_car].insert(0,
                            [list_out_image[idx], ocr_output[idx], list_score_ocr[idx], list_box[idx], list_score_detect[idx], time_str])
                        break
                    else:
                        continue

                self.dict_car_tracking[key_car].append(
                    [ocr_output[idx], list_score_ocr[idx], list_box[idx], list_score_detect[idx], time_str])

            self.dict_idcam_start[id_camera][1] = time
            self.dict_idcam_start[id_camera][2] = y_tl

        else:
            NEW_CAR = [key_car]
            # time start a car, time start of last car, y topleft when car disappear
            self.dict_idcam_start[id_camera] = [time, time, y_tl]
            ct = str(time).replace("-", "").replace(":",
                                                    "").replace(" ", "") #.split(".")[0]
            new_id_car = "_".join([id_camera, ct])

            # log
            # if (self.convert_second(time) - self.convert_second(last_time)) > self.DEALTA_TIME:
            #     self.logger.info(f"diff time: {self.convert_second(time) - self.convert_second(last_time)}")
            # elif abs(y_tl - last_y_tl) > self.DEALTA_Y:
            #     self.logger.info(f"delta y: {abs(y_tl - last_y_tl)}")
            self.dict_car_tracking[new_id_car] = [
                [list_out_image[idx], ocr_output[idx], list_score_ocr[idx], list_box[idx], list_score_detect[idx], ct]]
        return NEW_CAR

    def check_new_car(self, list_queue, list_box, idx, list_out_image, ocr_output, list_score_ocr, list_score_detect):
        """checking new car and cache result

        Args:
            list_queue (list): list value of queue
            list_box (list): list bounding box  
            idx (int): index of frame in queue
            list_out_image (list): list image after infer
            ocr_output (list): list output ocr of image in queue
            list_score_ocr (list): list of score ocr

        Returns:
            bool: True if newcar else False
        """
        keep_check = self.take_init_car(list_queue, list_box, idx,
                                        list_out_image, ocr_output, list_score_ocr, list_score_detect)
        if keep_check:
            NEW_CAR = self.tracking(list_queue, list_box, idx,
                                    list_out_image, ocr_output, list_score_ocr, list_score_detect)
        else:
            NEW_CAR = []
        return NEW_CAR
    
    def save_best_new_car(self, list_queue, list_box, idx, list_out_image, list_ocr_output, list_score_ocr, list_score_detect):
        NEW_CAR = self.check_new_car(list_queue, list_box, idx, list_out_image, list_ocr_output, list_score_ocr, list_score_detect)
        if NEW_CAR:
            best_result_list, best_result_topk_list, id_cam_list, value_list = get_best_result(NEW_CAR, self.dict_car_tracking, self.dict_idcam_start, self.VIN_config["max_e"], self.logger)
            if best_result_list:
                for i in range(len(best_result_list)):
                    if best_result_list[i]:
                        self.sql_object.sqlQueue.put([best_result_list[i], id_cam_list[i], value_list[i], NEW_CAR[i], self.Sql_VIN, self.VIN_config["path_out"], self.VIN_config["visualize"]])
                
                for i in range(len(best_result_topk_list)):
                    if best_result_topk_list[i]:
                        for j in range(len(best_result_topk_list[i])):
                            self.sql_object.sqlQueue.put([best_result_topk_list[i][j], id_cam_list[i], value_list[i], NEW_CAR[i], self.Sql_VIN_topk, self.VIN_config["path_out"], False])

    def check_out_time(self, threshold_time):
        """after threshold_time time will save last id_car

        Args:
            threshold_time (int): number second want to check

        Returns:
            list: list of last id_car need save
        """
        list_last_idcar = []
        list_id_car = self.dict_car_tracking.keys()
        time_now = datetime.datetime.now()
        if len(self.dict_idcam_start):
            for id_cam, value in self.dict_idcam_start.items():

                time_last = value[1]
                if (self.convert_second(time_now) - self.convert_second(time_last)) > threshold_time:
                    # self.logger.debug(f"check time out: {(self.convert_second(time_now) - self.convert_second(time_last))} \n {value} ([time when a car appear, time when a car disappear, ytop of last car])")
                    for id_car in list_id_car:
                        if id_cam in id_car:
                            list_last_idcar.append(id_car)

        return list_last_idcar
    
    def save_best_out_time(self):
        list_last_idcar = self.check_out_time(self.VIN_config["threshold_time"])
        dict_last_idcar = {}
        for i in list_last_idcar:
            name_camera = i.split("_")[0]
            if name_camera in self.dict_idcam_start:
                self.dict_idcam_start.pop(name_camera)

        best_result_list, best_result_topk_list, id_cam_list, value_list = get_best_result(list_last_idcar, self.dict_car_tracking, self.dict_idcam_start, self.VIN_config["max_e"], self.logger)
        if best_result_list:
            for i in range(len(best_result_list)):
                if best_result_list[i]:
                    self.sql_object.sqlQueue.put([best_result_list[i], id_cam_list[i], value_list[i], list_last_idcar[i], self.Sql_VIN, self.VIN_config["path_out"], self.VIN_config["visualize"]])
            for i in range(len(best_result_topk_list)):
                    if best_result_topk_list[i]:
                        for j in range(len(best_result_topk_list[i])):
                            self.sql_object.sqlQueue.put([best_result_topk_list[i][j], id_cam_list[i], value_list[i], list_last_idcar[i], self.Sql_VIN_topk, self.VIN_config["path_out"], False])