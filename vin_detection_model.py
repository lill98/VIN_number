import os
import shutil
import sys
from tkinter import N
from unittest import result
import cv2
from numpy import half, imag
import torch
import torch.backends.cudnn as cudnn
import numpy as np
from models.common import DetectMultiBackend
from utils.datasets import IMG_FORMATS, VID_FORMATS, LoadImages, LoadStreams, ProcessImage
from utils.general import (LOGGER, check_file, check_img_size, check_imshow, check_requirements, colorstr,
                           increment_path, non_max_suppression, non_max_suppression_obb, print_args, scale_coords, scale_polys, strip_optimizer, xyxy2xywh)
from utils.plots import Annotator, colors, save_one_box
from utils.torch_utils import select_device, time_sync
from utils.rboxs_utils import poly2rbox, rbox2poly
import numpy as np
from utils.perspective_transform import list_2_points, four_point_transform
from paddleocr import PaddleOCR, draw_ocr
from utils.cv2_puttext_wrapper import draw_text
import glob
import tqdm
import time
from lib_multicam.tools import clean_string
from utils.config import Cfg

# debug_delete
from threading import Thread

class Save_debugimage():
    def __init__(self) -> None:
        pass
    def save_image(self, image, name_image, path_out):
        thread = Thread(target=self.save, args=(image, name_image, path_out))
        thread.start()
    def save(self, image, name_image, path_out = "out_debug_image"):
        if not os.path.exists(path_out):
            os.mkdir(path_out)
        path_image = os.path.join(path_out, name_image)
        cv2.imwrite(path_image, image)

# top debug_delete

class VinDetModel():
    """infer model detection
    """

    def __init__(self, weights, imgsz, device="0", conf=0.25, iou_thres=0.45, max_det=1000) -> None:
        self.device = select_device(device)
        weights = [weights]
        self.model = DetectMultiBackend(weights, device=self.device, dnn=False)
        self.stride, self.names, self.pt, self.jit, self.onnx, self.engine = \
            self.model.stride, self.model.names, self.model.pt, self.model.jit, self.model.onnx, self.model.engine
        imgsz *= 2 if len(imgsz) == 1 else 1  # expand
        imgsz = check_img_size(imgsz, s=self.stride)  # check image size
        # half precision only supported by PyTorch on CUDA
        half = False
        half &= (self.pt or self.jit or self.engine) and self.device.type != 'cpu'
        if self.pt or self.jit:
            self.model.model.half() if half else self.model.model.float()
        self.model.warmup(imgsz=(1, 3, *imgsz), half=half)
        # self.preprocess = ProcessImage(imgsz, self.stride, self.pt)
        self.preprocess = ProcessImage(imgsz, self.stride, False)

        self.conf_thres = conf
        self.iou_thres = iou_thres
        self.max_det = max_det
        self.half = half

    def infer(self, list_image, padding_detect=10):
        """function infer detect

        Args:
            list_image (list): list of image
            padding (int, optional): padding want to pad. Defaults to 10.

        Returns:
            tuple: result and drawed image after infer
        """
        batch_image = []
        list_ori_img = []
        for img in list_image:
            img = self.preprocess.preprocess_img(img)
            img = torch.from_numpy(img).to(self.device)
            img = img.half() if self.half else img.float()  # uint8 to fp16/32
            img /= 255  # 0 - 255 to 0.0 - 1.0
            if len(img.shape) == 3:
                img = img[None]  # expand for batch dim
                batch_image.append(img.to("cpu"))
            list_ori_img.append(img)

        batch_image = torch.from_numpy(
            np.concatenate(batch_image, axis=0)).to(self.device)
        pred = self.model(batch_image)
        pred = non_max_suppression_obb(pred, self.conf_thres, self.iou_thres,
                                       multi_label=True, max_det=self.max_det)
        results = []
        # print(pred)
        # det = torch.ones((0,1))
        for ori_img, img, det in zip(list_image, list_ori_img, pred):
            if 0 in det.size():
                results.append((None, None))
                continue
            det[:, 2:4] = det[:, 2:4] + padding_detect
            pred_poly = rbox2poly(det[:, :5])
            if len(det):
                pred_poly = scale_polys(
                    img.shape[2:], pred_poly, ori_img.shape)
                # (n, [poly conf cls])
                det_ = torch.cat((pred_poly, det[:, -2:]), dim=1)
                min_conf = 0

                for element in det_:
                    if element[8] > min_conf:
                        det = [element.tolist()]
                        min_conf = element[8]
                        
                pts = list_2_points(det[0][0:8])
                conf = det[0][8]
                results.append((pts, conf))
        return results, list_image


class InferBatch():
    """Infer detection and ocr model
    """
    def __init__(self, VIN_config) -> None:
        self.VIN_config = VIN_config
        # DEFINE YOLO MODEL CONFIG
        self.ocr = PaddleOCR(use_angle_cls=False,\
                    rec_model_dir=self.VIN_config['rec_model_pth'],\
                    det_model_dir=self.VIN_config['det_model_pth'],\
                    lang='en', show_log=False,\
                    det_db_unclip_ratio=self.VIN_config['padding_ocr'],\
                    ocr_version=self.VIN_config['ocr_version'])
        # DEFINE PPOCR MODEL CONFIG
        self.vinDetModel = VinDetModel(
                    self.VIN_config['yolo_model_pth'], \
                    [self.VIN_config["size_infer"]], \
                    conf = self.VIN_config["conf"], \
                    iou_thres=self.VIN_config["iou_thres"])  # 1280 self, weights, imgsz, device="0", conf=0.25, iou_thres=0.45, max_det=1000

        self.padding_detect = self.VIN_config["padding_det"]
        image = np.ones((1280,1280,3))
        for i in range(3):
            self.vinDetModel.infer([image], self.padding_detect)

    def infer_batch(self, list_image, list_id_camera, list_timestamp):
        """function infer detection and ocr model

        Args:
            list_image (list): list of image

        Returns:
            tuple: list image, list ocr, list detected box, list confident score of ocr
        """
        list_out_image = []
        list_box = []
        list_ocr = []
        list_score_ocr = []
        list_score_detect = []

        dets, list_ori_img = self.vinDetModel.infer(list_image, self.padding_detect)
        # return None, None, None, None, None

        if dets is None or not len(dets):
            return None, None, None, None, None

        for (pts, conf), img, id_cam, time_stamp in zip(dets, list_ori_img, list_id_camera, list_timestamp):

            if conf == 0 or conf is None:
                list_box.append(None)
                list_out_image.append(None)
                list_ocr.append(None)
                list_score_ocr.append(None)
                list_score_detect.append(None)
            
            else:
                vin_img, rect = four_point_transform(img, pts)
                vin_img = cv2.cvtColor(vin_img, cv2.COLOR_BGR2RGB)
                list_box.append(rect)
                result = self.ocr.ocr(vin_img, cls=False)
                torch.cuda.synchronize
                vin_img = cv2.cvtColor(vin_img, cv2.COLOR_RGB2BGR)
                txts = [line[1][0] for line in result]
                len_txts = [len(line[1][0]) for line in result]
                scores = [line[1][1] for line in result]
                
                # find max 
                if len(len_txts):
                    max_len = max(len_txts)
                    txts = [txts[len_txts.index(max_len)]]
                    scores = [scores[len_txts.index(max_len)]]
                    
                for index, (txt, score) in enumerate(zip(txts, scores)):
                    txts[index] = clean_string(txt)

                # debug_delete
                if self.VIN_config["path_out_debug"]:
                    img_draw = img.copy()
                    draw_txt = ''
                    for txt, score in zip(txts, scores):
                        draw_txt += "ocr: " + txt + "\n"
                        draw_txt += "ocr_conf: " + str(score) + "\n"
                    draw_txt += "det_conf: " + str(conf)

                    img_draw = cv2.polylines(img_draw, np.int32(
                        [pts]), True, color=(0, 255, 0), thickness=2)
                    img_draw = draw_text(img_draw, draw_txt, tuple(
                        np.int32(pts[1])), fontScale=1, color=(0, 0, 255), thickness=2)
                    name_image = "_".join([id_cam, str(time_stamp).replace("-", "").replace(":", "").replace(" ", "")]) + ".jpg"
                    Save_debugimage().save_image(img_draw, name_image, self.VIN_config["path_out_debug"])
                
                list_out_image.append(img)
                list_score_detect.append(conf)

                if len(txts):
                    list_ocr.append(txts[0])
                    list_score_ocr.append(scores[0])
                else:
                    list_ocr.append(None)
                    list_score_ocr.append(None)

        return list_out_image, list_ocr, list_box, list_score_ocr, list_score_detect


if __name__ == "__main__":
    VIN_config = Cfg.load_config_from_file("config/VIN_config.yaml")
    Infer_VIN = InferBatch(VIN_config)
    input_folder = "out_multicam/best_result/*"
    output_folder = 'output_test'    # For baseline task
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    else:
        shutil.rmtree(output_folder)
        os.mkdir(output_folder)

    result_ocr = open(os.path.join(output_folder, "result.txt"), 'w')

    for input_img_path in tqdm.tqdm(glob.glob(input_folder)):
        print("infer image: ", input_img_path)
        name_image = input_img_path.split("/")[-1]
        img = cv2.imread(input_img_path)
        # print("read img:", time.time() - s1)
        list_out_image, list_ocr_output, list_box, list_score_ocr, list_score_detect = Infer_VIN.infer_batch([img],["cam1"])
        # basename = os.path.basename(input_img_path)

        for ocr_output in list_ocr_output:
            if ocr_output:
                result_ocr.write(input_img_path + ": " + ocr_output + "\n")
    result_ocr.close()
