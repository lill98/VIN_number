# VIN ocr 
> This project about detecting VIN number from the car (windshield)

## Pre-Installation
The project is currently being tested on a server with these specs:
> - OS: Ubunutu/Windows
> - CUDA: 11.3
> - Cudnn: 8.2.1
> - Visual Studio 2019
> - Anaconda (Specifically to create virtual env)

## Installation
The project currently uses these packages:
> - Python: 3.8.13
> - Pytorch: 1.10.0 (ver. CUDA 11.3)
> - PaddlePaddle-GPU: 2.2.2 (ver. CUDA 11.2)
> - OpenCV: 4.5.5.64

After installing [Anaconda](https://www.anaconda.com/products/individual), we can set up the virtual environment for this project

**1. Create a new conda environment**:
```bash
$ conda create --name VIN python=3.8.13
```
And activate the environment
```bash
$ conda activate VIN
```

**2. Install the requirements**

Python 3.8 with all requirements.txt dependencies installed. 
* For Ubuntu run:
```bash
$ pip install -r requirements.txt
$ cd utils/nms_rotated
$ python setup.py develop  #or "pip install -v -e ."
```
* For Windows run:
```bash
$ pip install -r requirements_windows.txt
$ cd utils/nms_rotated
$ python setup.py develop  #or "pip install -v -e ."
```

## Setup The Restart Feature

**1. Setup a Google Cloud project for email sending feature**

The user could refer to this [link](https://developers.google.com/workspace/guides/create-project) for detailed instruction:

1. Use an existing Gmail account or setup a dedicated Gmail account;

2. Visit the [Google Cloud console](https://console.cloud.google.com/);

3. At the top-left, click menu > **IAM & Admin** > **Create a Project**;

   <img src="resources/gmail_create_project.png" alt="gmail_create_project" style="zoom:50%;" />



4. Give a **Project Name**. Please remember this project name as we will use it for later steps;

5. Click **Create**

**2. Setup a credential**

A credential is needed for our application to interact with Google APIs. We have to go all through this painful setup process as Google has tightened security policies that prevent third-party applications to access Google APIs.

1. Click the top left menu button > **APIs & Services** > **Credentials**

   <img src="resources/gmail_create_credential.png" alt="gmail_create_credential" style="zoom:50%;" />

2. On the **Credentials** screen, click the top bar button **CREATE CREDENTIALS** > **OAuth client ID**. The console will require the user to setup the **OAuth consent screen**;

3. On the **OAuth consent screen** setup: 1) choose **User type** as **External** (we could choose **Internal** if the Google account is enterprise type); 2) fill the project name into the **app name** (or else we could not pass this screen, we do not know why); 3) add the username (email) into **Test users** list (since we use this app internally). Go through the process to save the consent screen;

4. Go back to the **Credentials** screen, click **CREATE CREDENTIALS** > **OAuth client ID**, choose **Application type** as **Desktop app**. We could give it a name, then click **Create**

   <img src="resources/gmail_desktop_app.png" alt="gmail_desktop_app" style="zoom:50%;" />

5. At the end of the process, a popup will appear, click **Download JSON**, and save the file under the name "credentials.json" into the **vin_restart** directory;

   <img src="resources/gmail_credential_created.png" alt="gmail_credential_created" style="zoom:50%;" />

**3. First-time Login** 

For first-time login, please delete any `token.json` file in the `vin_restart` directory. The process will generate a new `token.json` file which will be used to authenticate requests from the app.

Afterwards, please run the following command in the `vin_restart` directory:

```
python -m gmail_api.first_time_login
```

A browser window will pop up and point to a Google sign in page:
   <img src="resources/auth_login.png" alt="auth_first_time_login" style="zoom:50%;" />

Sign in using your working Gmail account. You could click **Continue** if prompted by Google that *Google hasn’t verified this app*, and click **Continue** untill the end.

A new `token.json` file will be saved into the working directory. We are good to go.

**4. Run the standalone restart feature**

The Restart program could be run independently of the main app.

Windown:
```
cmd /K vin_restart\run_autorestart.bat
```

## Change config files
Important fields to config are:
> - source: Change the RTSP link inputs, and choose whether it is 180° rotated or not.
> - batch_infer: Maximum of images processed every batch. This should be set higher if GPU RAM is higher for faster processing.
> - skip_frame: Number of frame skipped. (If set to 3 --> Only get 1 image every 3 images inputing).
> - size_queue: The size of the image queue. Set higher if processing speed < image reading speed.
> - threshold_time: number of seconds waited until saving the result in database. (Set higher to make sure not having duplicate in database).
> - database information: should be set based on MySql data.

## Log file reading
Important rows to look at log file:
> - The log file begins with showing all the configs with Yolov5-Obb. Afterward, each rows will follow the format below:
> - {Timestamp} - root - INFO:
>   - FPS queue (speed of reading images)
>   - FPS program (speed of processing images)
>   - Query successful on {CamID} {OCR Result} (Successfully saved result in database)
> - {Timestamp} - root - DEBUG:
>   - Cant read image from {CamID} (Stream is dead)
>   - check time out (our AI dev's debug)

## Pipeline Visualization
[Pipeline](./static/Pipeline.png)

## References
> + PaddleOCR (OCR model): https://www.paddlepaddle.org.cn/
> + Yolov5_OBB (object detection): https://github.com/hukaixuan19970627/yolov5_obb