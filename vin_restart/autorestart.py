import logging, logging.config
from autorestart.autorestart import AutoRestart
from gmail_api.gmail_ready import GmailObject
from gmail_api import SCOPES
import argparse

TOKEN_FILE = './/vin_restart//token.json'
CRED_FILE = './/vin_restart//credentials.json'

if __name__ == '__main__':
    # logging.config.fileConfig('logging.conf')
    # logger = logging.getLogger('autorestart')

    parser = argparse.ArgumentParser()
    parser.add_argument('--logfile', type = str, help = 'Path to log file')
    parser.add_argument('--pidfile', type = str, help = 'Path to PID file')
    parser.add_argument('--processfile', type = str, help = 'Path to process file')
    parser.add_argument('--receiver', type = str, help = 'Email address to receive notification')

    args = parser.parse_args()
    try:
        gmail_api = GmailObject.create_gmail_object(TOKEN_FILE, SCOPES)
        message_attrs = {
            'receiver': args.receiver,
            'subject': 'Error',
            'message': 'RTSP stream might have died.'
        }

        autorestart = AutoRestart(
            log_file = args.logfile,
            pid_file = args.pidfile,
            process_path = args.processfile,
            restart_kw = 'NEED_RESTART',
            restart_threshold = 2,
            restart_interval = 10*60
        )

        autorestart.loop(gmail_api, message_attrs)
    except Exception as e:
        with open("eror.txt","w") as f:
            f.write(str(e))