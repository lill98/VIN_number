# https://developers.google.com/gmail/api/guides/sending
from __future__ import print_function
from gmail_api import SCOPES, TOKEN_FILE, SENDER, RECEIVER

import base64
from email.message import EmailMessage

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

class GmailObject():
    def __init__(self, creds, service, token_file) -> None:
        """
        Return an object that supports sending email through Gmail APIs.
        """
        self.creds = creds
        self.service = service
        self.token_file = token_file

    @classmethod
    def create_gmail_object(cls, token_file = TOKEN_FILE, scopes = SCOPES):
        creds = Credentials.from_authorized_user_file(token_file, scopes)
        try:
            service = build('gmail', 'v1', credentials = creds)
            return cls(creds, service, TOKEN_FILE)
        except:
            print(f'Failed to create a service from the provided {TOKEN_FILE} file and {SCOPES} scopes.')
            return None
    
    def check_cred_valid(self):
        if not self.creds or not self.creds.valid:
            if self.creds and self.creds.expired and self.creds.refresh_token:
                return False
        return True

    def refresh_service(self):
        # Refresh cred if expired
        self.creds.refresh(Request())
        try:
            self.service = build('gmail', 'v1', credentials = self.creds)
        except:
            print(f'Failed to create a service from the given credentials.')
            return None

        # Save the credentials for the next run
        with open(self.token_file, 'w') as token:
            token.write(self.creds.to_json())
            
    def send_mail(self, receiver, subject, content):
        # if self.check_cred_valid == False:
        #     self.refresh_service()
        
        try:
            message = EmailMessage()
            message.set_content(content)

            message['To'] = receiver
            message['From'] = SENDER
            message['Subject'] = subject

            # encoded message
            encoded_message = base64.urlsafe_b64encode(message.as_bytes()).decode()
            create_message = {
                'raw': encoded_message
            }
            # pylint: disable=E1101
            send_message = (self.service.users().messages().send
                            (userId="me", body=create_message).execute())
            print(F'Message Id: {send_message["id"]}')
        except HttpError as error:
            print(F'An error occurred: {error}')
            send_message = None
        return send_message

if __name__ == '__main__':
    gmail = GmailObject.create_gmail_object()
    if gmail is not None:
        gmail.send_mail(RECEIVER, 'Test email', 'Hello there! --> General Kenobiii <3!')