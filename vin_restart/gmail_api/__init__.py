# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/gmail.compose']
TOKEN_FILE = './/token.json'
CRED_FILE = './/credentials.json'

SENDER = 'solus161dev@gmail.com'
RECEIVER = 'solus161@gmail.com'