import os, sys
import time
import subprocess
from subprocess import CalledProcessError
from datetime import datetime
import logging
import threading

LOG_FILE = '../logs.txt'
PROCESS_PATH = '../test_run_app.py'
RESTART_KW = 'NEED_RESTART'
PID_FILE = 'log_PID'
RESTART_THRESHOLD = 2           # Restart if the rtsp link fails more than twice
RESTART_INTERVAL = 10 * 60      # within 10 mins
TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S'

def check_pid(pid: str, platform = 'win'):
    # Check whether the platform is win or linux
    if platform == 'win':
        command = f'tasklist /FI "PID eq {pid}"'
        try:
            output = subprocess.run(command, shell = False, check = True, stdout = subprocess.PIPE)
            if 'No tasks are running' in output.stdout.decode():
                return False
            else:
                return True
        except CalledProcessError as e:
            logging.error(e)
            return False
    elif platform == 'linux':
        # TODO: To be implemented
        running = False
    
    return running

def kill_pid(pid: str, platform = 'win'):
    command = None
    if platform == 'win':
        command = f'taskkill /F /PID {pid}'
    elif platform == 'linux':
        command = f'kill -9 {pid}'

    try:
        output = subprocess.run(command, shell = False, check = True)
        return True if output.returncode == 0 else False
    except CalledProcessError as e:
        logging.error(e)
        return False

class AutoRestart():
    def __init__(self, log_file, pid_file, process_path, restart_kw, \
                restart_threshold = RESTART_THRESHOLD, \
                restart_interval = RESTART_INTERVAL):
        self.window = []
        self.log_file = log_file
        self.pid_file = pid_file
        self.process_path = process_path
        self.restart_kw = restart_kw
        self.restart_threshold = restart_threshold
        self.restart_interval = restart_interval

    def check_log(self, file_encoding = 'utf-8'):
        """
        Read log file from the last line.
        Check for keyword in the beginning of the last line.

        Return datetime object and True or False.
        """
        if file_encoding == 'ascii':
            # Only ascii-encoded file allow searching from the end of file
            with open(self.log_file, 'rb') as f:
                try:
                    f.seek(-2, os.SEEK_END)
                    while f.read(1) != b'\n':
                        f.seek(-2, os.SEEK_CUR)
                except OSError:
                    f.seek(0)
                last_line = f.readline().decode()
        elif file_encoding == 'utf-8':
            # This way is slower for large files, but work for both ascii and utf-8 encoded file
            with open(self.log_file, 'r') as f:
                try:
                    last_line = f.readlines()[-1]

                    if self.restart_kw in last_line:
                        # Extract timestamp from
                        # 2022-06-20 10:27:22,754 - root - DEBUG - NEED_RESTART
                        last_line = last_line.split(' ')
                        timestamp = ' '.join(last_line[:2]).split(',')[0]
                        timestamp = datetime.strptime(timestamp, TIMESTAMP_FORMAT)
                        return timestamp, True
                except IndexError:
                    pass

        return None, False

    def restart(self, pid: str, wait_time = 1):
        """
        Try to kill the process having pid, wait for 1 sec, and restart.
        """
        platform = None
        if 'win' in sys.platform:
            platform = 'win'
        elif 'linux' in sys.platform:
            platform = 'linux'

        killed = kill_pid(pid, platform)
        # running = False
        # while running == False:
        #     running = check_pid(pid, platform)
        #     time.sleep(wait_time)
        
        if killed == False:
            # Could use logging
            logging.error(f'Failed to kill process with pid {pid}. Please check again.')
            return False
        
        restart_command = f'python {self.process_path}'
        try:
            output = subprocess.Popen(restart_command, shell = False)
            return True
        except CalledProcessError as e:
            logging.error(e)
            return False

    def loop(self, gmail_obj, message_attrs = None, check_interval = 0.5):
        """
        Loop and check for rtsp link's health once per sec.
        """
        previous_restart = None
        while True:
            timestamp, inactive = self.check_log()
            if inactive == True:    
                if timestamp not in self.window and timestamp != previous_restart:
                    if len(self.window) > self.restart_threshold:
                        self.window.pop(0)

                    self.window.append(timestamp)
                    logging.info(f'Catch new {self.restart_kw} at {datetime.strftime(timestamp, TIMESTAMP_FORMAT)}')
                else:
                    time.sleep(check_interval)
                    continue
                
                # Restart
                try:
                    with open(self.pid_file, 'r') as f:
                        pid = f.readline()
                except FileNotFoundError as e:
                    logging.error(e)
                    continue
                
                success = self.restart(pid)
                previous_restart = timestamp
                if success == False:
                    logging.error(f'Failed to restart the process at {self.process_path}, pid of {pid}.')
                    break
                logging.info(f'Successfully restart process at {self.process_path}')

                # Check 3 times restart to send email
                if len(self.window) > self.restart_threshold:
                    timedelta = timestamp - self.window[0]
                    timedelta = timedelta.seconds

                    if timedelta <= self.restart_interval:
                        logging.info('{} times restart within {} and {}'.format(
                                        self.restart_threshold + 1,
                                        datetime.strftime(timestamp, TIMESTAMP_FORMAT),
                                        datetime.strftime(self.window[0], TIMESTAMP_FORMAT)))
                        logging.info(f'Sending mail to {message_attrs["receiver"]} ...')
                        # Send mail to notify admin
                        if gmail_obj is not None:
                            try:
                                gmail_obj.send_mail(
                                    message_attrs['receiver'], 
                                    message_attrs['subject'], 
                                    message_attrs['message'])
                            except Exception as e:
                                logging.error(e)
                                # The token.json may expire, need to refresh the token
                                logging.error(f'The {gmail_obj.token_file} may expire, refreshing the access token ...')
                                # gmail_obj.refresh_service()
                                # gmail_obj.send_mail(
                                #     message_attrs['receiver'], 
                                #     message_attrs['subject'], 
                                #     message_attrs['message'])
                        self.window = []
                else:
                    time.sleep(check_interval)
                    continue
            time.sleep(check_interval)