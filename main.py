import argparse
from lib_multicam.tools import *
from lib_multicam.read_stream import *
from lib_multicam.sql_stream import *
from vin_detection_model import InferBatch
from lib_multicam.tracking_count_time import *
from utils.config import Cfg

import time

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-p", "--path-config", type=str, default="config/VIN_config.yaml",
                help="path to config file")

args = ap.parse_args()


def main(args):
    logger = Log("log.txt")
    pid = os.getpid()
    with open("log_pid.txt", "w") as f:
        f.write(str(pid))
    # logger_pid = Log("log_pid.txt")
    
    VIN_config = Cfg.load_config_from_file(args.path_config)
    assert VIN_config is not None
    logger.info(f"Config: {VIN_config}")

    source_link = {}
    for dict_POT in VIN_config["source"]:
        for pot, cam_value in dict_POT.items():
            source_link.update(cam_value)

    print(VIN_config)
    make_dir(VIN_config["path_out"])
    list_thread = []
    dict_idcam_start = {}
    dict_car_tracking = {}

    Infer_VIN = InferBatch(VIN_config)
    Sql_VIN = Sql(VIN_config, VIN_config["name_table"], logger)
    Sql_VIN_topk = Sql(VIN_config, VIN_config["name_table_debug"])
    sqlQueue = Queue(VIN_config["size_sql_queue"])
    workQueue = Queue(VIN_config["size_queue"])
    sql_object, thread_sql = SqlQueue(sqlQueue, name_Thread="SQL").start()
    tracking = Tracking(VIN_config, Sql_VIN, Sql_VIN_topk, sql_object, dict_idcam_start, dict_car_tracking, logger)

    # init thread
    for name_camera, value in source_link.items():
        source = value[0]["link"]
        ROTATE = value[1]["rotate"]
        print(f"[INFO] sampling THREADED frames from webcam {source}")
        vs, thread = WebcamVideoStream(
            VIN_config, workQueue, src=source, name_Thread=name_camera, ROTATE=ROTATE, logger=logger).start()
        list_thread.append(vs)
    
    for thead in list_thread:
        thead.update_flag()

    time_1 = time.time()
    start_size = workQueue.qsize()

    keep = True
    while keep:
        time0 = time.time()
        if 4 < time.time() - time_1 < 6:
            logger.info(f"FPS queue: {(workQueue.qsize() - start_size)/5}")
            start_size = workQueue.qsize()
            time_1 = time.time()
            keep = False

    count = 0
    count_time = 0
    while True:
        try:
            time0 = time.time()
            list_queue = vs.take_queue(VIN_config["batch_infer"])
            if len(list_queue):
                list_image = [i[0] for i in list_queue]
                list_id_camera = [i[1] for i in list_queue]
                list_timestamp = [i[2] for i in list_queue]

                list_out_image, list_ocr_output, list_box, list_score_ocr, list_score_detect = Infer_VIN.infer_batch(list_image, list_id_camera, list_timestamp)
            
                if not list_out_image:
                    tracking.save_best_out_time()
                    continue

                for idx, image in enumerate(list_out_image):
                    if image is None:
                        tracking.save_best_out_time()
                        continue

                    # find new car
                    tracking.save_best_new_car(list_queue, list_box, idx, list_out_image, list_ocr_output, list_score_ocr, list_score_detect)
                
                # calculate fps program
                if count < 4:
                    count += 1
                    count_time += len(list_queue)/(time.time() - time0)
                elif count == 4:
                    logger.info(f"FPS program: {count_time/4}")
                    count += 1


        except Exception as e:
            logger.debug(f"Program kill, check fail:  {e}")
            logger.debug("NEED_RESTART")

                
if __name__ == '__main__':
    main(args)
